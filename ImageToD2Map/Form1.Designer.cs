﻿namespace ImageToD2Map
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBoxRes1 = new System.Windows.Forms.PictureBox();
            this.buttonOpen = new System.Windows.Forms.Button();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonProcess = new System.Windows.Forms.Button();
            this.buttonSave2 = new System.Windows.Forms.Button();
            this.pictureBoxRes2 = new System.Windows.Forms.PictureBox();
            this.pictureBox_k_means_1 = new System.Windows.Forms.PictureBox();
            this.pictureBox_k_means_2 = new System.Windows.Forms.PictureBox();
            this.buttonSave3 = new System.Windows.Forms.Button();
            this.pictureBoxRes3 = new System.Windows.Forms.PictureBox();
            this.comboBoxSize = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBoxRotate = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRes1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRes2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_k_means_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_k_means_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRes3)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox1.Location = new System.Drawing.Point(6, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(350, 381);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBoxRes1
            // 
            this.pictureBoxRes1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxRes1.Location = new System.Drawing.Point(638, 10);
            this.pictureBoxRes1.Name = "pictureBoxRes1";
            this.pictureBoxRes1.Size = new System.Drawing.Size(144, 144);
            this.pictureBoxRes1.TabIndex = 1;
            this.pictureBoxRes1.TabStop = false;
            // 
            // buttonOpen
            // 
            this.buttonOpen.Location = new System.Drawing.Point(13, 13);
            this.buttonOpen.Name = "buttonOpen";
            this.buttonOpen.Size = new System.Drawing.Size(75, 23);
            this.buttonOpen.TabIndex = 2;
            this.buttonOpen.Text = "Open";
            this.buttonOpen.UseVisualStyleBackColor = true;
            this.buttonOpen.Click += new System.EventHandler(this.buttonOpen_Click);
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave.Location = new System.Drawing.Point(788, 107);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(75, 23);
            this.buttonSave.TabIndex = 3;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonProcess
            // 
            this.buttonProcess.Location = new System.Drawing.Point(362, 170);
            this.buttonProcess.Name = "buttonProcess";
            this.buttonProcess.Size = new System.Drawing.Size(75, 60);
            this.buttonProcess.TabIndex = 5;
            this.buttonProcess.Text = "Process";
            this.buttonProcess.UseVisualStyleBackColor = true;
            this.buttonProcess.Click += new System.EventHandler(this.buttonProcess_Click);
            // 
            // buttonSave2
            // 
            this.buttonSave2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave2.Location = new System.Drawing.Point(788, 261);
            this.buttonSave2.Name = "buttonSave2";
            this.buttonSave2.Size = new System.Drawing.Size(75, 23);
            this.buttonSave2.TabIndex = 7;
            this.buttonSave2.Text = "Save";
            this.buttonSave2.UseVisualStyleBackColor = true;
            this.buttonSave2.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBoxRes2
            // 
            this.pictureBoxRes2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxRes2.Location = new System.Drawing.Point(638, 160);
            this.pictureBoxRes2.Name = "pictureBoxRes2";
            this.pictureBoxRes2.Size = new System.Drawing.Size(144, 144);
            this.pictureBoxRes2.TabIndex = 6;
            this.pictureBoxRes2.TabStop = false;
            // 
            // pictureBox_k_means_1
            // 
            this.pictureBox_k_means_1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_k_means_1.Location = new System.Drawing.Point(488, 159);
            this.pictureBox_k_means_1.Name = "pictureBox_k_means_1";
            this.pictureBox_k_means_1.Size = new System.Drawing.Size(144, 144);
            this.pictureBox_k_means_1.TabIndex = 8;
            this.pictureBox_k_means_1.TabStop = false;
            // 
            // pictureBox_k_means_2
            // 
            this.pictureBox_k_means_2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox_k_means_2.Location = new System.Drawing.Point(488, 309);
            this.pictureBox_k_means_2.Name = "pictureBox_k_means_2";
            this.pictureBox_k_means_2.Size = new System.Drawing.Size(144, 144);
            this.pictureBox_k_means_2.TabIndex = 11;
            this.pictureBox_k_means_2.TabStop = false;
            // 
            // buttonSave3
            // 
            this.buttonSave3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSave3.Location = new System.Drawing.Point(788, 411);
            this.buttonSave3.Name = "buttonSave3";
            this.buttonSave3.Size = new System.Drawing.Size(75, 23);
            this.buttonSave3.TabIndex = 10;
            this.buttonSave3.Text = "Save";
            this.buttonSave3.UseVisualStyleBackColor = true;
            this.buttonSave3.Click += new System.EventHandler(this.buttonSave3_Click);
            // 
            // pictureBoxRes3
            // 
            this.pictureBoxRes3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxRes3.Location = new System.Drawing.Point(638, 310);
            this.pictureBoxRes3.Name = "pictureBoxRes3";
            this.pictureBoxRes3.Size = new System.Drawing.Size(144, 144);
            this.pictureBoxRes3.TabIndex = 9;
            this.pictureBoxRes3.TabStop = false;
            // 
            // comboBoxSize
            // 
            this.comboBoxSize.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSize.FormattingEnabled = true;
            this.comboBoxSize.Items.AddRange(new object[] {
            "144",
            "96",
            "72",
            "48"});
            this.comboBoxSize.Location = new System.Drawing.Point(362, 143);
            this.comboBoxSize.Name = "comboBoxSize";
            this.comboBoxSize.Size = new System.Drawing.Size(75, 21);
            this.comboBoxSize.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(362, 117);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Map Size:";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.checkBoxRotate);
            this.groupBox1.Controls.Add(this.buttonProcess);
            this.groupBox1.Controls.Add(this.buttonSave3);
            this.groupBox1.Controls.Add(this.pictureBox_k_means_2);
            this.groupBox1.Controls.Add(this.buttonSave2);
            this.groupBox1.Controls.Add(this.pictureBoxRes3);
            this.groupBox1.Controls.Add(this.buttonSave);
            this.groupBox1.Controls.Add(this.comboBoxSize);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.pictureBoxRes2);
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.pictureBox_k_means_1);
            this.groupBox1.Controls.Add(this.pictureBoxRes1);
            this.groupBox1.Location = new System.Drawing.Point(12, 42);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(884, 464);
            this.groupBox1.TabIndex = 14;
            this.groupBox1.TabStop = false;
            // 
            // checkBoxRotate
            // 
            this.checkBoxRotate.AutoSize = true;
            this.checkBoxRotate.Location = new System.Drawing.Point(365, 251);
            this.checkBoxRotate.Name = "checkBoxRotate";
            this.checkBoxRotate.Size = new System.Drawing.Size(73, 17);
            this.checkBoxRotate.TabIndex = 14;
            this.checkBoxRotate.Text = "Rotate 45";
            this.checkBoxRotate.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(903, 515);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonOpen);
            this.Name = "Form1";
            this.Text = "ImgToD2Map";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRes1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRes2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_k_means_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_k_means_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRes3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBoxRes1;
        private System.Windows.Forms.Button buttonOpen;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonProcess;
        private System.Windows.Forms.Button buttonSave2;
        private System.Windows.Forms.PictureBox pictureBoxRes2;
        private System.Windows.Forms.PictureBox pictureBox_k_means_1;
        private System.Windows.Forms.PictureBox pictureBox_k_means_2;
        private System.Windows.Forms.Button buttonSave3;
        private System.Windows.Forms.PictureBox pictureBoxRes3;
        private System.Windows.Forms.ComboBox comboBoxSize;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox checkBoxRotate;
    }
}

