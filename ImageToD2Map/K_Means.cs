﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace ImageToD2Map
{
    
    public class Claster
    {
        public List<Color> data = new List<Color>();
        public Color center;

        public void UpdateCenter()
        {
            float r = 0, g = 0, b = 0;
            foreach (Color color in data)
            {
                r += color.R;
                g += color.G;
                b += color.B;
            }
            center = Color.FromArgb((int)r / data.Count, (int)g / data.Count, (int)b / data.Count);
        }
    }
    public class K_Means
    {
        public static double deltaColor(Color color1, Color color2)
        {
            return /*Math.Sqrt*/((color1.R - color2.R) * (color1.R - color2.R) +
                (color1.G - color2.G) * (color1.G - color2.G) +
                (color1.B - color2.B) * (color1.B - color2.B));
        }

        public List<Claster> clasters = new List<Claster>();
        int subStep = 0;
        int step = 0;
        //int stepLimit = 10;

        public void Reset()
        {
            subStep = 0;
            step = 0;
            clasters.Clear();
        }

        public void Finish()
        {
            for (int i = 0; i < clasters.Count; ++i)
                clasters[i].UpdateCenter();
        }

        public void AddClaster(Color color)
        {
            clasters.Add(new Claster() { center = color});
            clasters.Last().data.Add(color);
        }

        public int NearestClusterIndex(ref Color color)
        {
            int index = 0;
            double delta = deltaColor(clasters[0].center, color);
            for (int i = 1; i < clasters.Count; ++i)
            {
                double local = deltaColor(clasters[i].center, color);
                if (local < delta)
                {
                    delta = local;
                    index = i;
                }
            }
            return index;
        }

        public int NearestClusterIndex(Color color, ref List<int> indexes)
            
        {
            if (indexes.Count == 0)
                return 0;
            int index = 0;
            double delta = deltaColor(clasters[index].center, color);
            for (int i = 1; i < indexes.Count; ++i)
            {
                double local = deltaColor(clasters[indexes[i]].center, color);
                if (local < delta)
                {
                    delta = local;
                    index = i;
                }
            }
            return index;
        }

        public void Push(Color color)
        { 
            clasters[NearestClusterIndex(ref color)].data.Add(color);
            subStep++;
            if (subStep == clasters.Count)
            {
                step++;
                subStep = 0;
                Finish();
            }
        }
    }
}
