﻿using NevendaarTools;
using NevendaarTools.Creation;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using static NevendaarTools.DataTypes.D2MapBlock;

namespace ImageToD2Map
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            InitTyles();
            InitColors();
            comboBoxSize.SelectedIndex = 0;
            groupBox1.Enabled = false;

            buttonSave.Enabled = false;
            buttonSave2.Enabled = false;
            buttonSave3.Enabled = false;

            checkBoxRotate.Checked = settings.Value("rotate") == "True";
            comboBoxSize.SelectedIndex = int.Parse(settings.Value("mapSizeIndex", "0"));
        }

        public static Image ResizeImage(Image imgToResize, Size size)
        {
            return (Image)(new Bitmap(imgToResize, size));
        }

        public static Image RotateImage(Image img, float rotationAngle)
        {
            //create an empty Bitmap image
            Bitmap bmp = new Bitmap(img.Width, img.Height);

            //turn the Bitmap into a Graphics object
            Graphics gfx = Graphics.FromImage(bmp);

            //now we set the rotation point to the center of our image
            gfx.TranslateTransform((float)bmp.Width / 2, (float)bmp.Height / 2);

            //now rotate the image
            gfx.RotateTransform(rotationAngle);

            gfx.TranslateTransform(-(float)bmp.Width / 2, -(float)bmp.Height / 2);

            //set the InterpolationMode to HighQualityBicubic so to ensure a high
            //quality image once it is transformed to the specified size
            gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;

            //now draw our new image onto the graphics object
            gfx.DrawImage(img, new Point(0, 0));

            //dispose of our Graphics object
            gfx.Dispose();

            //return the image
            return bmp;
        }

        public static double deltaColor(Color color1, Color color2)
        {
            return /*Math.Sqrt*/((color1.R - color2.R) * (color1.R - color2.R) +
                (color1.G - color2.G) * (color1.G - color2.G) +
                (color1.B - color2.B) * (color1.B - color2.B));
        }

        List<Color> colors = new List<Color>();
        List<int> tileValues = new List<int>();
        Image image;
        int mapSize = 96;
        List<MapCreator> creators = new List<MapCreator>() { new MapCreator(), new MapCreator(), new MapCreator() };
        string name = "";
        SettingsManager settings = new SettingsManager("options.xml");

        void InitTyles()
        {
            tileValues.Add(1);
            tileValues.Add(2);
            tileValues.Add(3);
            tileValues.Add(4);
            tileValues.Add(5);
            tileValues.Add(6);

            tileValues.Add(29);
           // tileValues.Add((int)TileType.NeutralsTree);
        }

        void InitColors()
        {
            colors.Add(Color.FromArgb(19, 50, 13));//empire
            colors.Add(Color.FromArgb(217, 214, 209));//clans
            colors.Add(Color.FromArgb(29, 4, 0));//demons
            colors.Add(Color.FromArgb(43, 39, 29));//undeads
            colors.Add(Color.FromArgb(69, 49, 14));//elves
            colors.Add(Color.FromArgb(50, 44, 33));//neutrals

            colors.Add(Color.FromArgb(28, 72, 78));//water
            //colors.Add(Color.FromArgb(72, 73, 36));//tree
        }

        int TileIndexByColor(Color color)
        {
            int index = 0;
            double minDelta = deltaColor(color, colors[0]);
            for (int i = 1; i < colors.Count; ++i)
            {
                double delta = deltaColor(colors[i], color);
                if (delta < minDelta)
                {
                    minDelta = delta;
                    index = i;
                }
            }

            return index;
        }


        private void buttonOpen_Click(object sender, EventArgs e)
        {
            mapSize = int.Parse(comboBoxSize.Text);
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Файлы изображений|*.bmp;*.png;*.jpg";
            openDialog.InitialDirectory = settings.Value("openPath", ".");
            if (openDialog.ShowDialog() != DialogResult.OK)
                return;

            try
            {
                image = Image.FromFile(openDialog.FileName);
            }
            catch (OutOfMemoryException ex)
            {
                MessageBox.Show("Ошибка чтения картинки");
                return;
            }
            name = Path.GetFileNameWithoutExtension(openDialog.FileName);
            settings.SetValue("openPath", Path.GetDirectoryName(openDialog.FileName));
            pictureBox1.Image = image;
            groupBox1.Enabled = true;
        }

        Image PreparedImage()
        {
            Image res = ResizeImage(image, new Size(mapSize, mapSize));
            if (checkBoxRotate.Checked)
                res = RotateImage(res, -45);
            return res;
        }

        Image VisibleImage(Image input)
        {
            if (checkBoxRotate.Checked)
                return RotateImage(input, 45);
            return input;
        }

        MapCreator.MapGenSettings GetMapSettings()
        {
            MapCreator.MapGenSettings settings = new MapCreator.MapGenSettings();
            settings.name = name;
            settings.author = "ImgToD2Map generator";
            settings.races.Add(GameRace.Empire);
            settings.size = mapSize;
            return settings;
        }

        private void processSimple()
        {
            mapSize = int.Parse(comboBoxSize.Text);
            Image img = PreparedImage();
            MapCreator.MapGenSettings settings = GetMapSettings();

            MapCreator creator = creators[0];
            creator.Create(settings);
            Bitmap bmp = new Bitmap(img);
            Bitmap res = new Bitmap(mapSize, mapSize);
            for (int i = 0; i < mapSize; i++)
            {
                for (int k = 0; k < mapSize; k++)
                {
                    Color color = bmp.GetPixel(i, k);
                    int index = TileIndexByColor(color);
                    creator._grid[i, k].value = tileValues[index];
                    res.SetPixel(i, k, colors[index]);
                }
            }
            creator.Finish();
            pictureBoxRes1.Image = VisibleImage(res);
        }

        private void processK_Means_1()
        {
            Image img = PreparedImage();
            MapCreator.MapGenSettings settings = GetMapSettings();

            MapCreator creator = creators[1];
            creator.Create(settings);
            Bitmap bmp = new Bitmap(img);
            Bitmap res = new Bitmap(mapSize, mapSize);
            Bitmap reskMeans = new Bitmap(mapSize, mapSize);
            K_Means kmeans = new K_Means();

            List<int> indexes = new List<int>();
            Dictionary<int, int> colorIndexes = new Dictionary<int, int>();
            for (int i = 0; i < colors.Count; ++i)
            {
                kmeans.AddClaster(colors[i]);
                indexes.Add(i);
            }
            for (int i = 0; i < mapSize; i++)
            {
                for (int k = 0; k < mapSize; k++)
                {
                    Color color = bmp.GetPixel(i, k);
                    kmeans.Push(color);
                }
            }
            kmeans.Finish();

            for (int i = 0; i < mapSize; i++)
            {
                for (int k = 0; k < mapSize; k++)
                {
                    Color color = bmp.GetPixel(i, k);
                    int clasterIndex = kmeans.NearestClusterIndex(ref color);
                    creator._grid[i, k].value = tileValues[clasterIndex];
                    reskMeans.SetPixel(i, k, kmeans.clasters[clasterIndex].center);
                    res.SetPixel(i, k, colors[clasterIndex]);
                }
            }

            pictureBoxRes2.Image = VisibleImage(res);
            pictureBox_k_means_1.Image = VisibleImage(reskMeans);
            creator.Finish();
        }

        Color colorByPos(ref Bitmap bmp, int x, int y)
        {
            int xMin = Math.Max(0, x - 1), xMax = Math.Min(mapSize - 1, x + 1);
            int yMin = Math.Max(0, y - 1), yMax = Math.Min(mapSize - 1, y + 1);
            float r = 0, g = 0, b = 0;
            int count = (xMax - xMin + 1) * (yMax - yMin + 1);

            for (int i = xMin; i <= xMax; i++)
            {
                for (int k = yMin; k <= yMax; k++)
                {
                    if (i == x && k == y)
                        continue;
                    Color color = bmp.GetPixel(i, k);
                    r += color.R;
                    g += color.G;
                    b += color.B;
                }
            }
            r /= (count - 1);
            g /= (count - 1);
            b /= (count - 1);

            Color baseColor = bmp.GetPixel(x, y);
            float baseValue = 0.4f;
            r = r * (1 - baseValue) + baseColor.R * baseValue;
            g = g * (1 - baseValue) + baseColor.G * baseValue;
            b = b * (1 - baseValue) + baseColor.B * baseValue;

            return Color.FromArgb((int)r, (int)g , (int)b);
        }

        private void processK_Means_2()
        {
            Image img = PreparedImage();
            MapCreator.MapGenSettings settings = GetMapSettings();

            MapCreator creator = creators[2];
            creator.Create(settings);
            Bitmap bmp = new Bitmap(img);
            Bitmap res = new Bitmap(mapSize, mapSize);
            Bitmap reskMeans = new Bitmap(mapSize, mapSize);
            K_Means kmeans = new K_Means();

            List<int> indexes = new List<int>();
            Dictionary<int, int> colorIndexes = new Dictionary<int, int>();
            for (int i = 0; i < colors.Count; ++i)
            {
                kmeans.AddClaster(colors[i]);
                indexes.Add(i);
            }
            for (int i = 0; i < mapSize; i++)
            {
                for (int k = 0; k < mapSize; k++)
                {
                    Color color = bmp.GetPixel(i, k); //colorByPos(ref bmp, i, k);
                    kmeans.Push(color);
                }
            }
            kmeans.Finish();

            for (int i = 0; i < mapSize; i++)
            {
                for (int k = 0; k < mapSize; k++)
                {
                    Color color = colorByPos(ref bmp, i, k);
                    int clasterIndex = kmeans.NearestClusterIndex(ref color);
                    creator._grid[i, k].value = tileValues[clasterIndex];
                    reskMeans.SetPixel(i, k, kmeans.clasters[clasterIndex].center);
                    res.SetPixel(i, k, colors[clasterIndex]);
                }
            }

            pictureBoxRes3.Image = VisibleImage(res);
            pictureBox_k_means_2.Image = VisibleImage(reskMeans);
            creator.Finish();
        }

        private void buttonProcess_Click(object sender, EventArgs e)
        {
            processSimple();
            processK_Means_1();
            processK_Means_2();
            buttonSave.Enabled = true;
            buttonSave2.Enabled = true;
            buttonSave3.Enabled = true;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.FileName = name + "_1.sg";
            saveFileDialog1.InitialDirectory = settings.Value("savePath", ".");
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;
            settings.SetValue("savePath", Path.GetDirectoryName(saveFileDialog1.FileName));
            creators[0].SaveMap(saveFileDialog1.FileName);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.FileName = name + "_2.sg";
            saveFileDialog1.InitialDirectory = settings.Value("savePath", ".");
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            settings.SetValue("savePath", Path.GetDirectoryName(saveFileDialog1.FileName));
            creators[1].SaveMap(saveFileDialog1.FileName);
        }

        private void buttonSave3_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.FileName = name + "_3.sg";
            saveFileDialog1.InitialDirectory = settings.Value("savePath", ".");
            if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                return;

            settings.SetValue("savePath", Path.GetDirectoryName(saveFileDialog1.FileName));
            creators[2].SaveMap(saveFileDialog1.FileName);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            settings.SetValue("rotate", checkBoxRotate.Checked ? "True" : "False");
            settings.SetValue("mapSizeIndex", comboBoxSize.SelectedIndex.ToString());
        }
    }
}
